from pyexpat import model
from django.db import models
from django.forms import ModelForm

# Create your models here.


class SignUpForm(ModelForm):
    username = models.CharField(max_length=16)
