from receipts.views import (
    AccountsCreateView,
    CategoriesCreateView,
    CategoriesListView,
    ReceiptCreateView,
    ReceiptListView,
    AccountsListView,
)


from django.urls import path

urlpatterns = [
    path("", ReceiptListView.as_view(), name="home"),
    path("create/", ReceiptCreateView.as_view(), name="receipt_create"),
    path("accounts/", AccountsListView.as_view(), name="accounts_view"),
    path(
        "accounts/create", AccountsCreateView.as_view(), name="accounts_create"
    ),
    path("categories/", CategoriesListView.as_view(), name="expense_list"),
    path(
        "categories/create",
        CategoriesCreateView.as_view(),
        name="expense_create",
    ),
]
