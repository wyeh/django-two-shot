from django.contrib import admin
from receipts.models import Account, Receipt, ExpenseCategory

# here we are wanting to import the MODELS, CREATE ADMIN CLASS for the model, then register with admin site with the model and the admin class we just made

# Register your models here.
class AccountAdmin(admin.ModelAdmin):
    pass


class ReceiptAdmin(admin.ModelAdmin):
    pass


class CategoryAdmin(admin.ModelAdmin):
    pass


admin.site.register(Account, AccountAdmin)
admin.site.register(Receipt, ReceiptAdmin)
admin.site.register(ExpenseCategory, CategoryAdmin)
